#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

. $DIR/env.sh

if [ ! -d $ARTIFACTORY_DATA ]; then
    echo -n Creating Artifactory Data Directory...
    mkdir -p $ARTIFACTORY_DATA
    echo Done
elif [[ $1 != P ]]; then
    echo -n Clearing Artifactory Data Directory...
    rm -rf $ARTIFACTORY_DATA/*
    echo Done
fi

if [ ! -d $ARTIFACTORY_APP ]; then
    echo -n Creating Artifactory Application Directory...
    mkdir -p $ARTIFACTORY_APP
    echo Done
elif [[ $1 != P ]]; then
    echo -n Clearing Artifactory Application Directory...
    rm -rf $ARTIFACTORY_APP/*
    echo Done
fi

if [ ! -d $NGINX ]; then
    echo -n Creating NGINX Directory...
    mkdir -p $NGINX
    echo Done
else
    echo -n Clearing NGINX Directory...
    rm -rf $NGINX/*
    echo Done
fi
cp -R $DIR/configuration/nginx/* $NGINX/.

if [ ! -d $JENKINS ]; then
    echo -n Creating Jenkins Home Directory...
    mkdir -p $JENKINS
    echo Done
elif [[ $1 != P ]]; then
    echo -n Clearing Jenkins Home Directory...
    rm -rf $JENKINS/*
    echo Done
fi

if [ ! -d $BITBUCKET ]; then
    echo -n Creating Bitbucket Home Directory...
    mkdir -p $BITBUCKET
    echo Done
elif [[ $1 != P ]]; then
    echo -n Clearing Bitbucket Home Directory...
    rm -rf $BITBUCKET/*
    echo Done
fi

if [ ! -d $BITBUCKETDB_DATA ]; then
    echo -n Creating Bitbucket Data Directory...
    mkdir -p $BITBUCKETDB_DATA
    echo Done
elif [[ $1 != P ]]; then
    echo -n Clearing Bitbucket Data Directory...
    rm -rf $BITBUCKETDB_DATA/*
    echo Done
fi

if [ ! -d $JIRADB_DATA ]; then
    echo -n Creating JIRA Data Directory...
    mkdir -p $JIRADB_DATA
    echo Done
elif [[ $1 != P ]]; then
    echo -n Clearing JIRA Data Directory...
    rm -rf $JIRADB_DATA/*
    echo Done
fi

if [ ! -d $JIRA ]; then
    echo -n Creating JIRA Home Directory...
    mkdir -p $JIRA
    echo Done
elif [[ $1 != P ]]; then
    echo -n Clearing JIRA Home Directory...
    rm -rf $JIRA/*
    echo Done
fi

if [ ! -d $SONARDB_DATA ]; then
    echo -n Creating Sonar Data Directory...
    mkdir -p $SONARDB_DATA
    echo Done
elif [[ $1 != P ]]; then
    echo -n Clearing Sonar Data Directory...
    rm -rf $SONARDB_DATA/*
    echo Done
fi

if [ ! -d $SONAR_PLUGINS ]; then
    echo -n Creating Sonar Plugins Directory...
    mkdir -p $SONAR_PLUGINS
    echo Done
elif [[ $1 != P ]]; then
    echo -n Clearing Sonar Plugins Directory...
    rm -rf $SONAR_PLUGINS/*
    echo Done
fi

docker stack deploy -c alm.yml alm